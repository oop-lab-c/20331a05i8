public class MethodOLJava {
    void sum(int a,int b) {
        System.out.println("sum = "+(a+b));
    }

    void sum(int a,int b,int c) {
        System.out.println("sum = "+(a+b+c));
    }

    public static void main(String args[]) {
        MethodOLJava obj = new MethodOLJava();
        obj.sum(3,4);
        obj.sum(3,4,5);
    }
}