interface clg {
    void display();
}
class PureAbsJava implements clg {
    public void display() {
        System.out.println("my clg name is mvgr");
    }
    public static void main(String[] args) {
        PureAbsJava obj = new PureAbsJava();
       obj.display();
    }
}
