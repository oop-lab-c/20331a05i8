import java.util.*;
class MarksException extends Exception{
    MarksException(String msg){
        System.out.println(msg);
    }
}
class Main {
        public static void main(String[] args) {
        Scanner obj=new Scanner(System.in); 
         System.out.print("Enter your marks :"); 
        try{
            int marks=obj.nextInt();
            student(marks);
        }catch(Exception e){
            System.out.println(e);
        }finally{
            System.out.println("finally  block executed");
        }
        }
        public static void student(int m) throws MarksException{
        if(m<40){
            throw  new MarksException("student is failed in examination");
        }
        else
        System.out.println("student is promoted to next class");
        }
       
        
    }  
